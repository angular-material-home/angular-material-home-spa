# SPA module of AMH

[![pipeline status](https://gitlab.com/angular-material-home/angular-material-home-spa/badges/master/pipeline.svg)](https://gitlab.com/angular-material-home/angular-material-home-spa/commits/master)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/419f7e67ea7c4946aeece48a215b9a42)](https://www.codacy.com/app/angular-material-home/angular-material-home-spa?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=angular-material-home/angular-material-home-spa&amp;utm_campaign=Badge_Grade)
[![Codacy Badge](https://api.codacy.com/project/badge/Coverage/419f7e67ea7c4946aeece48a215b9a42)](https://www.codacy.com/app/angular-material-home/angular-material-home-spa?utm_source=gitlab.com&utm_medium=referral&utm_content=angular-material-home/angular-material-home-spa&utm_campaign=Badge_Coverage)
