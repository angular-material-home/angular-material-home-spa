/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

/*
 * 
 */
angular.module('ngMaterialHomeSpa', [
	'ngMaterialHome'
]);
/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
angular.module('ngMaterialHomeSpa')

/**
 * @ngdoc controller
 * @name AmhSpasCtrl
 * @description List of all spas
 * 
 * Manages list of all SPAS
 * 
 */
.controller('AmhSpasCtrl', function($scope, $q, $tenant, $controller) {
	// Extends items controller
	angular.extend(this, $controller('MbSeenAbstractCollectionCtrl', {
		$scope: $scope
	}));

	// Overried the function
	this.getModelSchema = function(){
		return $tenant.spaSchema();
	};

	// get accounts
	this.getModels = function(parameterQuery){
		return $tenant.getSpas(parameterQuery);
	};

	// get an spa
	this.getModel = function(id){
		return $tenant.getSpa(id);
	};

	// delete spa
	this.deleteModel = function(item){
		return $tenant.deleteSpa(item.id);
	};

	// init super controller
	this.init({
		eventType: '/tenant/sapas'
	});
});
/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialHomeSpa')
/*
 * Adds module menu
 */
.run(function($actions, $navigator) {
	$actions.newAction({
		icon: 'apps',
		title: 'Applications',
		description: 'List of installed applications',
		action: function(){
			return $navigator.openDialog({
				templateUrl : 'views/dialogs/amh-installed-spas.html',
				config : {
				}
			});
		},
		groups:['amh.owner-toolbar.public']
	});
});
angular.module('ngMaterialHomeSpa').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('views/dialogs/amh-installed-spas.html',
    "<md-dialog ng-controller=\"AmhSpasCtrl as ctrl\" ng-cloak> <md-dialog-content layout=row layout-align=\"start start\" layout-wrap layout-padding mb-infinate-scroll=ctrl.loadNextPage() flex> <a ng-repeat=\"spa in ctrl.items track by spa.id\" ng-href=\"/{{::spa.name}}/\" target=_blank style=\"display: block; text-decoration: none; font-size: 10px; color: inherit\" layout=column> <img width=64px ng-src=/{{::spa.name}}/images/logo.svg> <h3 translate=\"\">{{::spa.title}}</h3> </a> </md-dialog-content> </md-dialog>"
  );

}]);
